Welcome to Git repo for Linux Firewalls and Routers.

Steps to follow

1. Install Vagrant, VirtualBox, and an openssh client (On Windows, msysgit has this package).
2. In the directory containing Vagrantfile, run Vagrant up.

Please find the PPT here:

http://null.co.in/2014/04/14/linux-routing-and-firewalls-for-beginners/
